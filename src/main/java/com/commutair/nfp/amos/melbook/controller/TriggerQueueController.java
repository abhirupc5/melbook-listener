package com.commutair.nfp.amos.melbook.controller;

import java.net.URI;
import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.messaging.config.annotation.EnableSqs;
import org.springframework.cloud.aws.messaging.listener.SqsMessageDeletionPolicy;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.RestTemplate;

import com.commutair.nfp.amos.melbook.domain.Response;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@EnableSqs
public class TriggerQueueController {
	
	private Logger logger = LoggerFactory.getLogger(TriggerQueueController.class);
	
	@Autowired
	private RestTemplate template;
	
	@Autowired
	private ObjectMapper mapper;
	
	@Value("${melbook-queue-url}")
	private String queueName;
	
	@Value("${transformer-endpoint-url}")
	private String transformerEndpoint;
	
	@Value("${post-transformer-endpoint-url}")
	private String postTransformerEndpoint;
	
	@SqsListener(deletionPolicy = SqsMessageDeletionPolicy.ON_SUCCESS, value = "${melbook-queue-url}")
	public void listen(final String message, @Headers MessageHeaders headers) throws Exception {
		final String uniqueMessageId = (String) headers.get("MessageId");
		MDC.put("correlationId", uniqueMessageId);
		logger.info(String.format("Message from queue %s --> %s", queueName.split("/")[queueName.split("/").length - 1], message));
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		httpHeaders.set("correlationId", uniqueMessageId);

		HttpEntity<String> entity = new HttpEntity<>(message, httpHeaders);
		ResponseEntity<String> transformResponse = template.exchange(new URI(transformerEndpoint), HttpMethod.POST, entity, String.class);
		logger.info(String.format("Response from transformer (%s) --> %s", uniqueMessageId, transformResponse.getBody()));
		Response responseFromTransformer = mapper.readValue(transformResponse.getBody(), Response.class);
		if(HttpStatus.resolve(responseFromTransformer.getStatus()) == HttpStatus.ACCEPTED) {
			logger.info("Transformed response -- " + responseFromTransformer.getMessage());
			
			httpHeaders = new HttpHeaders();
			httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			httpHeaders.set("correlationId", uniqueMessageId);

			entity = new HttpEntity<>(responseFromTransformer.getMessage(), httpHeaders);
			transformResponse = template.exchange(new URI(postTransformerEndpoint), HttpMethod.POST, entity, String.class);
			logger.info(String.format("Response from transformer (%s) --> %s", uniqueMessageId, transformResponse.getBody()));
			responseFromTransformer = mapper.readValue(transformResponse.getBody(), Response.class);
			
			if(HttpStatus.resolve(responseFromTransformer.getStatus()) != HttpStatus.ACCEPTED) {
				throw new Exception();
			}
		}
	}
}
